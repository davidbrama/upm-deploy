#!/usr/bin/env bash
#
# Required globals:
#   URL
#   USERNAME
#   PASSWORD
#   FILENAME
# Optional globals:
# DEBUG (default: "false")

source "$(dirname "$0")/common.sh"

info "uploading to $URL"

enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
  fi
}
enable_debug


DEBUG=${DEBUG:="false"}
url=$URL/rest/plugins/1.0/
token=$(curl -sI "$url?os_authType=basic" --user $USERNAME:$PASSWORD | grep upm-token | cut -d: -f2- | tr -d "[[:space:]]")

debug "Got token $token"

curl --user $USERNAME:$PASSWORD -XPOST "$url?token=$token" -F plugin=@$FILENAME
